# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table predio (
  id                        bigint not null,
  nome                      varchar(255),
  numero                    bigint,
  endereco                  varchar(255),
  campus                    varchar(255),
  andar                     integer,
  constraint pk_predio primary key (id))
;

create table sala (
  id                        bigint not null,
  numero                    integer,
  tipo                      varchar(255),
  capacidade                integer,
  predio_id                 bigint,
  constraint pk_sala primary key (id))
;

create table account (
  email                     varchar(255) not null,
  name                      varchar(255),
  password                  varchar(255),
  constraint pk_account primary key (email))
;

create sequence predio_seq;

create sequence sala_seq;

create sequence account_seq;

alter table sala add constraint fk_sala_predio_1 foreign key (predio_id) references predio (id) on delete restrict on update restrict;
create index ix_sala_predio_1 on sala (predio_id);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists predio;

drop table if exists sala;

drop table if exists account;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists predio_seq;

drop sequence if exists sala_seq;

drop sequence if exists account_seq;

