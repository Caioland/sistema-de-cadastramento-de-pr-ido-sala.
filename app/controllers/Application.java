package controllers;

import models.User;
import play.*;
import play.data.Form;
import play.mvc.*;

import views.html.*;

public class Application extends Controller {

	//formulario para fazer login
	static Form<Login> formLogin = Form.form(Login.class);

	/*
	 * ACTIONS DO CONTROLLERS
	 */

	

	/*
	 * METODOS E CLASSES PARA LOGIN
	 */
	// -- Classe para autenticacao
	public static class Login {

		// dados necessarios para login
		public String email;
		public String password;

		// AQUI ESTA O TRUQUE, COMO VALIDAR UM FORMULARIO
		// NA HORA EM QUE O FORMULARIO É VALIDADO, EXECUTAMOS O LOGIN!
		// O metodo validade é invocado quando o framework play valida o
		// formulario de login
		//Observe que ele invoca o metodo authenticate do model User
		//e nao o authenticate que encontra-se neste controller
		public String validate() {
			if (User.authenticate(email, password) == null) {
				return "Usuario ou senha inválidos";
			}
			return null;
		}

	} // -- fim da classe login

	/**
	 * Login page.
	 */
	public static Result login() {
		return ok(login.render(formLogin));
	}

	/**
	 * Atua na verificacao do formulario do login
	 */
	public static Result authenticate() {
		
		//recebe formulario de login
		Form<Login> loginForm = formLogin.bindFromRequest();
		//valida o formulario e, ao mesmo tempo, se o usuario conseguiu se autenticar
		if (loginForm.hasErrors()) {
			flash("error", "Login ou senha inválida. Tente novamente");
			//caso nao, envia novamente para o usuario
			return badRequest(login.render(loginForm));
		} else {
			//usuario autenticado
			//posso inserir dados na sessão
	
			session("connected", loginForm.get().email);
			
			//rediciono ele para a pagina inicial
			return redirect(routes.Admin.index());
		}
	}

	/**
	 * Logout and clean the session.
	 */
	public static Result logout() {
		session().clear();
		flash("success", "Sessão finalizada com sucesso.");
		return redirect(routes.Admin.index());
	}
	
	
	

}
