package controllers;


import java.util.List;


import models.Predio;
import models.Sala;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.index;
import views.html.components.sobre;


//a anotation a seguir informa que todas as actions deste controllers
//devem ser feitas apenas por usuarios autenticados
/**
 * Esta classe é um controller no qual todas as suas actions sao 
 * permitidas apenas para usuarios autenticados.
 * 
 * Isto é feito com uso da anotation abaixo
 */
@Security.Authenticated(Secured.class)
public class Admin extends Controller {
	static Form<Sala> formSala = Form.form(Sala.class);
	static Form<Predio> formPredio = Form.form(Predio.class);

	
    //CONTROLLERS PRECEDIDOS DE LOGIN
  
	public static Result index() {
		return ok(index.render(formPredio, formSala));
	}
	

   
   public static Result addsala() {
	   Form<Sala> formulario = formSala.bindFromRequest();
	   Sala novaSala = formulario.get();
	   novaSala.save();
	   flash("success", "Sala cadastrada com sucesso!");
	   return redirect("/");
   }
	   
	   public static Result addpredio() {
		   Form<Predio> formulario = formPredio.bindFromRequest();
		   Predio novoPredio = formulario.get();
		   novoPredio.save();
		   flash("success", "Predio cadastrado com sucesso!");
		   return redirect("/");
    
	   }
	   
	   
	   public static Result sobre() {
	   return ok(views.html.components.sobre.render());
	   }
	   
	  
	   
	   public static Result detalhaSala(Long id) {
		   Sala salaescolhida = Sala.find.byId(id);
		   return ok(views.html.components.sala.render(salaescolhida));
	   }
	   
	   public static Result editaSala(Long id) {
		   Sala salaescolhida = Sala.find.byId(id);
		   Form<Sala> form = formSala.fill(salaescolhida);
		   return ok(views.html.components.editaSala.render(form, salaescolhida));
		   
	   }
	   
	   public static Result updateSala(Long id) {
		   Form<Sala> formAlterar = formSala.bindFromRequest();
		   formAlterar.get().update(id);
		   flash("success", "Sala atualizada com sucesso!");
		   return redirect(routes.Admin.detalhaSala(id));
		   
	   }
	   
	   public static Result deletaSala(Long id) {
		   Sala salaEscolhida = Sala.find.byId(id);
		   salaEscolhida.delete();
		   flash("success", "Sala Removida com sucesso!");
		   return redirect(routes.Admin.index());
		   
	   }
	   
	   public static Result detalhaPredio(Long id) {
		   Predio predioescolhido = Predio.find.byId(id);
		   return ok(views.html.components.predio.render(predioescolhido));
	   }
	   
	   public static Result editaPredio(Long id) {
		   Predio predioescolhido = Predio.find.byId(id);
		   Form<Predio> form = formPredio.fill(predioescolhido);
		   return ok(views.html.components.editaPredio.render(form, predioescolhido));
		   
	   }
	   
	   public static Result updatePredio(Long id) {
		   Form<Predio> formAlterar = formPredio.bindFromRequest();
		   formAlterar.get().update(id);
		   flash("success", "Sala atualizada com sucesso!");
		   return redirect(routes.Admin.detalhaPredio(id));
		   
	   }
	   
	   public static Result deletaPredio(Long id) {
		   Predio predioEscolhido = Predio.find.byId(id);
		   if (models.Sala.find.byId(id) != null) {
		   flash("error", "Impossivel remover cadastro com chave estrangeira existente em outro model.");
		   return found(routes.Admin.index());
		   }
		   
		   else {
		   predioEscolhido.delete();
		   flash("success", "Predio removido com sucesso!");
		   return redirect ("/");
		   
		   }
	   }
}
