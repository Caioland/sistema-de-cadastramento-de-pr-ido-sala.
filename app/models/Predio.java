package models;
import java.util.ArrayList;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
public class Predio extends Model {
	
	@Id
	public Long id;
	public String nome;
	public Long numero;
	public String endereco;
	public String campus;
	public int andar;
	
	@OneToMany(mappedBy = "predio", cascade = CascadeType.ALL)
	public ArrayList<Sala> salas;
	
	//Finder
	public static Model.Finder<Long,Predio> find = new Model.Finder(Long.class, Predio.class);
	
	
}