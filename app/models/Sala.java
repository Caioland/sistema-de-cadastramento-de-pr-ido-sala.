package models;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
public class Sala extends Model {
	
	@Id
	public Long id;
	public int numero;
	public String tipo;
	public int capacidade;
	
	@ManyToOne
	public Predio predio;
	
	//Finder
	public static Model.Finder<Long,Sala> find = new Model.Finder(Long.class, Sala.class);
}