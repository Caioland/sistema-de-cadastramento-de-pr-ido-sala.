// @SOURCE:C:/Users/Caio/Desktop/Sistema-Monitoramento 3/conf/routes
// @HASH:a1a6ef6bb943b6797d9e7fee3d2bc0b91c2cac0f
// @DATE:Fri Nov 13 00:14:30 BRST 2015


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:6
private[this] lazy val controllers_Admin_index0 = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
        

// @LINE:9
private[this] lazy val controllers_Application_login1 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login"))))
        

// @LINE:10
private[this] lazy val controllers_Application_authenticate2 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login"))))
        

// @LINE:11
private[this] lazy val controllers_Application_logout3 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("logout"))))
        

// @LINE:12
private[this] lazy val controllers_Admin_sobre4 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("sobre"))))
        

// @LINE:13
private[this] lazy val controllers_Admin_addsala5 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addsala"))))
        

// @LINE:14
private[this] lazy val controllers_Admin_addsala6 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addsala"))))
        

// @LINE:15
private[this] lazy val controllers_Admin_addpredio7 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addpredio"))))
        

// @LINE:16
private[this] lazy val controllers_Admin_addpredio8 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addpredio"))))
        

// @LINE:17
private[this] lazy val controllers_Admin_detalhaSala9 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("sala/"),DynamicPart("id", """[^/]+""",true),StaticPart("/view"))))
        

// @LINE:18
private[this] lazy val controllers_Admin_editaSala10 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("sala/"),DynamicPart("id", """[^/]+""",true),StaticPart("/edit"))))
        

// @LINE:19
private[this] lazy val controllers_Admin_updateSala11 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("sala/"),DynamicPart("id", """[^/]+""",true),StaticPart("/update"))))
        

// @LINE:20
private[this] lazy val controllers_Admin_deletaSala12 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("sala/"),DynamicPart("id", """[^/]+""",true),StaticPart("/del"))))
        

// @LINE:21
private[this] lazy val controllers_Admin_detalhaPredio13 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("predio/"),DynamicPart("id", """[^/]+""",true),StaticPart("/view"))))
        

// @LINE:22
private[this] lazy val controllers_Admin_editaPredio14 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("predio/"),DynamicPart("id", """[^/]+""",true),StaticPart("/edit"))))
        

// @LINE:23
private[this] lazy val controllers_Admin_updatePredio15 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("predio/"),DynamicPart("id", """[^/]+""",true),StaticPart("/update"))))
        

// @LINE:24
private[this] lazy val controllers_Admin_deletaPredio16 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("predio/"),DynamicPart("id", """[^/]+""",true),StaticPart("/del"))))
        

// @LINE:26
private[this] lazy val controllers_Assets_at17 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
        
def documentation = List(("""GET""", prefix,"""controllers.Admin.index()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login""","""controllers.Application.login()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login""","""controllers.Application.authenticate()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """logout""","""controllers.Application.logout()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """sobre""","""controllers.Admin.sobre()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addsala""","""controllers.Admin.addsala()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addsala""","""controllers.Admin.addsala()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addpredio""","""controllers.Admin.addpredio()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addpredio""","""controllers.Admin.addpredio()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """sala/$id<[^/]+>/view""","""controllers.Admin.detalhaSala(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """sala/$id<[^/]+>/edit""","""controllers.Admin.editaSala(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """sala/$id<[^/]+>/update""","""controllers.Admin.updateSala(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """sala/$id<[^/]+>/del""","""controllers.Admin.deletaSala(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """predio/$id<[^/]+>/view""","""controllers.Admin.detalhaPredio(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """predio/$id<[^/]+>/edit""","""controllers.Admin.editaPredio(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """predio/$id<[^/]+>/update""","""controllers.Admin.updatePredio(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """predio/$id<[^/]+>/del""","""controllers.Admin.deletaPredio(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:6
case controllers_Admin_index0(params) => {
   call { 
        invokeHandler(controllers.Admin.index(), HandlerDef(this, "controllers.Admin", "index", Nil,"GET", """ Home page""", Routes.prefix + """"""))
   }
}
        

// @LINE:9
case controllers_Application_login1(params) => {
   call { 
        invokeHandler(controllers.Application.login(), HandlerDef(this, "controllers.Application", "login", Nil,"GET", """ Authentication""", Routes.prefix + """login"""))
   }
}
        

// @LINE:10
case controllers_Application_authenticate2(params) => {
   call { 
        invokeHandler(controllers.Application.authenticate(), HandlerDef(this, "controllers.Application", "authenticate", Nil,"POST", """""", Routes.prefix + """login"""))
   }
}
        

// @LINE:11
case controllers_Application_logout3(params) => {
   call { 
        invokeHandler(controllers.Application.logout(), HandlerDef(this, "controllers.Application", "logout", Nil,"GET", """""", Routes.prefix + """logout"""))
   }
}
        

// @LINE:12
case controllers_Admin_sobre4(params) => {
   call { 
        invokeHandler(controllers.Admin.sobre(), HandlerDef(this, "controllers.Admin", "sobre", Nil,"GET", """""", Routes.prefix + """sobre"""))
   }
}
        

// @LINE:13
case controllers_Admin_addsala5(params) => {
   call { 
        invokeHandler(controllers.Admin.addsala(), HandlerDef(this, "controllers.Admin", "addsala", Nil,"GET", """""", Routes.prefix + """addsala"""))
   }
}
        

// @LINE:14
case controllers_Admin_addsala6(params) => {
   call { 
        invokeHandler(controllers.Admin.addsala(), HandlerDef(this, "controllers.Admin", "addsala", Nil,"POST", """""", Routes.prefix + """addsala"""))
   }
}
        

// @LINE:15
case controllers_Admin_addpredio7(params) => {
   call { 
        invokeHandler(controllers.Admin.addpredio(), HandlerDef(this, "controllers.Admin", "addpredio", Nil,"GET", """""", Routes.prefix + """addpredio"""))
   }
}
        

// @LINE:16
case controllers_Admin_addpredio8(params) => {
   call { 
        invokeHandler(controllers.Admin.addpredio(), HandlerDef(this, "controllers.Admin", "addpredio", Nil,"POST", """""", Routes.prefix + """addpredio"""))
   }
}
        

// @LINE:17
case controllers_Admin_detalhaSala9(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Admin.detalhaSala(id), HandlerDef(this, "controllers.Admin", "detalhaSala", Seq(classOf[Long]),"GET", """""", Routes.prefix + """sala/$id<[^/]+>/view"""))
   }
}
        

// @LINE:18
case controllers_Admin_editaSala10(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Admin.editaSala(id), HandlerDef(this, "controllers.Admin", "editaSala", Seq(classOf[Long]),"GET", """""", Routes.prefix + """sala/$id<[^/]+>/edit"""))
   }
}
        

// @LINE:19
case controllers_Admin_updateSala11(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Admin.updateSala(id), HandlerDef(this, "controllers.Admin", "updateSala", Seq(classOf[Long]),"POST", """""", Routes.prefix + """sala/$id<[^/]+>/update"""))
   }
}
        

// @LINE:20
case controllers_Admin_deletaSala12(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Admin.deletaSala(id), HandlerDef(this, "controllers.Admin", "deletaSala", Seq(classOf[Long]),"GET", """""", Routes.prefix + """sala/$id<[^/]+>/del"""))
   }
}
        

// @LINE:21
case controllers_Admin_detalhaPredio13(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Admin.detalhaPredio(id), HandlerDef(this, "controllers.Admin", "detalhaPredio", Seq(classOf[Long]),"GET", """""", Routes.prefix + """predio/$id<[^/]+>/view"""))
   }
}
        

// @LINE:22
case controllers_Admin_editaPredio14(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Admin.editaPredio(id), HandlerDef(this, "controllers.Admin", "editaPredio", Seq(classOf[Long]),"GET", """""", Routes.prefix + """predio/$id<[^/]+>/edit"""))
   }
}
        

// @LINE:23
case controllers_Admin_updatePredio15(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Admin.updatePredio(id), HandlerDef(this, "controllers.Admin", "updatePredio", Seq(classOf[Long]),"POST", """""", Routes.prefix + """predio/$id<[^/]+>/update"""))
   }
}
        

// @LINE:24
case controllers_Admin_deletaPredio16(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Admin.deletaPredio(id), HandlerDef(this, "controllers.Admin", "deletaPredio", Seq(classOf[Long]),"GET", """""", Routes.prefix + """predio/$id<[^/]+>/del"""))
   }
}
        

// @LINE:26
case controllers_Assets_at17(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
   }
}
        
}

}
     