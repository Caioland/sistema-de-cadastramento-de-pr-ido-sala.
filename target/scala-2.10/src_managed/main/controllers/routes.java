// @SOURCE:C:/Users/Caio/Desktop/Sistema-Monitoramento 3/conf/routes
// @HASH:a1a6ef6bb943b6797d9e7fee3d2bc0b91c2cac0f
// @DATE:Fri Nov 13 00:14:30 BRST 2015

package controllers;

public class routes {
public static final controllers.ReverseAdmin Admin = new controllers.ReverseAdmin();
public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets();
public static final controllers.ReverseApplication Application = new controllers.ReverseApplication();
public static class javascript {
public static final controllers.javascript.ReverseAdmin Admin = new controllers.javascript.ReverseAdmin();
public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets();
public static final controllers.javascript.ReverseApplication Application = new controllers.javascript.ReverseApplication();
}
public static class ref {
public static final controllers.ref.ReverseAdmin Admin = new controllers.ref.ReverseAdmin();
public static final controllers.ref.ReverseAssets Assets = new controllers.ref.ReverseAssets();
public static final controllers.ref.ReverseApplication Application = new controllers.ref.ReverseApplication();
}
}
          