
package views.html.components

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object predio extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Predio,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(predio: Predio):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import helper.twitterBootstrap._


Seq[Any](format.raw/*1.18*/("""
"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Detalhes de um livro")/*5.30*/ {_display_(Seq[Any](format.raw/*5.32*/("""

<div class="page-header">
	<h1> Predio </h1>	
</div>

<h1> Campus: """),_display_(Seq[Any](/*11.15*/predio/*11.21*/.campus)),format.raw/*11.28*/(""" </h1>

<h3> Dados do Predio: </h3>

<ul>
	<li> <b> Nome: </b> """),_display_(Seq[Any](/*16.23*/predio/*16.29*/.nome)),format.raw/*16.34*/(""" </li>
	<li> <b> Numero: </b> """),_display_(Seq[Any](/*17.25*/predio/*17.31*/.numero)),format.raw/*17.38*/(""" </li>
	<li> <b> Endereco: </b> """),_display_(Seq[Any](/*18.27*/predio/*18.33*/.endereco)),format.raw/*18.42*/(""" </li>
	<li> <b> Campus: </b> """),_display_(Seq[Any](/*19.25*/predio/*19.31*/.campus)),format.raw/*19.38*/(""" </li>
	<li> <b> Andares: </b> """),_display_(Seq[Any](/*20.26*/predio/*20.32*/.andar)),format.raw/*20.38*/(""" </li>
</ul>

<a href="/">
	<span class="label label-succes">
	<i class="icon icon-arrow-left icon-white"></i> Voltar 
	</span>
</a>

""")))})))}
    }
    
    def render(predio:Predio): play.api.templates.HtmlFormat.Appendable = apply(predio)
    
    def f:((Predio) => play.api.templates.HtmlFormat.Appendable) = (predio) => apply(predio)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 13 00:14:32 BRST 2015
                    SOURCE: C:/Users/Caio/Desktop/Sistema-Monitoramento 3/app/views/components/predio.scala.html
                    HASH: d18c991f4224c980a208e44530da4df24308c724
                    MATRIX: 786->1|948->17|976->72|1013->75|1049->103|1088->105|1200->181|1215->187|1244->194|1349->263|1364->269|1391->274|1459->306|1474->312|1503->319|1573->353|1588->359|1619->368|1687->400|1702->406|1731->413|1800->446|1815->452|1843->458
                    LINES: 26->1|32->1|33->4|34->5|34->5|34->5|40->11|40->11|40->11|45->16|45->16|45->16|46->17|46->17|46->17|47->18|47->18|47->18|48->19|48->19|48->19|49->20|49->20|49->20
                    -- GENERATED --
                */
            