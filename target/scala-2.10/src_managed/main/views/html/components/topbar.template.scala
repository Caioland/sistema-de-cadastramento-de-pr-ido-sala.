
package views.html.components

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object topbar extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template0[play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply():play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.2*/("""<!-- BARRA TOPO -->
	
	
	
	
	
	<div class="navbar">
		<div class="navbar-inner"> 
			<li> <span class="label label-important"> FMU </span> <span class="label label-important"> SISTEMA DE CADASTRAMENTO </span> <p align="center"> <span class="label label-important">FMU<span class="badge">Sistema de cadastramento Predio-Sala</span></span> </li> </p>
			<!-- SPAN PARA LOGIN -->
			 <span class="navbar-text pull-right"> 
				"""),_display_(Seq[Any](/*12.6*/if(session.contains("connected"))/*12.39*/ {_display_(Seq[Any](format.raw/*12.41*/("""
			 		Bem vindo """),_display_(Seq[Any](/*13.18*/session/*13.25*/.get("connected"))),format.raw/*13.42*/(""" <a href=""""),_display_(Seq[Any](/*13.53*/routes/*13.59*/.Application.logout)),format.raw/*13.78*/(""""> <span class="label label-important"> Sair</span> </a>
				 """)))}/*14.7*/else/*14.12*/{_display_(Seq[Any](format.raw/*14.13*/("""
			  		<a  href=""""),_display_(Seq[Any](/*15.19*/routes/*15.25*/.Application.login)),format.raw/*15.43*/(""""> <span class="label label-important">Login <span>  </a> 
			 	""")))})),format.raw/*16.7*/("""

			 </span>
			 <!-- FIM SPAN PARA LOGIN -->
		<ul class="nav">
			 <li class="divider-vertical"></li>
			 
			
		</ul>
	</div>
	</div>		
	</p>
	
			 <!-- FIM BARRA TOPO -->"""))}
    }
    
    def render(): play.api.templates.HtmlFormat.Appendable = apply()
    
    def f:(() => play.api.templates.HtmlFormat.Appendable) = () => apply()
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 13 00:14:32 BRST 2015
                    SOURCE: C:/Users/Caio/Desktop/Sistema-Monitoramento 3/app/views/components/topbar.scala.html
                    HASH: 6608503fb79a9c551b7666ee5965dfabbbeb4bc8
                    MATRIX: 867->1|1327->426|1369->459|1409->461|1463->479|1479->486|1518->503|1565->514|1580->520|1621->539|1702->602|1715->607|1754->608|1809->627|1824->633|1864->651|1960->716
                    LINES: 29->1|40->12|40->12|40->12|41->13|41->13|41->13|41->13|41->13|41->13|42->14|42->14|42->14|43->15|43->15|43->15|44->16
                    -- GENERATED --
                */
            