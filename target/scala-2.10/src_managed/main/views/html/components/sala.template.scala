
package views.html.components

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object sala extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Sala,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(sala: Sala):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import helper.twitterBootstrap._


Seq[Any](format.raw/*1.14*/("""
"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Detalhes da Sala")/*5.26*/ {_display_(Seq[Any](format.raw/*5.28*/("""

<div class="page-header">
	<h1> Sala </h1>	
</div>

<h1> Sala: """),_display_(Seq[Any](/*11.13*/sala/*11.17*/.numero)),format.raw/*11.24*/(""" </h1>

<h3> Dados da Sala: </h3>

<ul>
	<li> <b> Numero: </b> """),_display_(Seq[Any](/*16.25*/sala/*16.29*/.numero)),format.raw/*16.36*/(""" </li>
	<li> <b> Tipo: </b> """),_display_(Seq[Any](/*17.23*/sala/*17.27*/.tipo)),format.raw/*17.32*/(""" </li>
	<li> <b> Capacidade: </b> """),_display_(Seq[Any](/*18.29*/sala/*18.33*/.capacidade)),format.raw/*18.44*/(""" </li>
	<li> <b> Predio: </b> """),_display_(Seq[Any](/*19.25*/Predio/*19.31*/.find.byId(sala.predio.id).nome)),format.raw/*19.62*/(""" </li>

</ul>

<a href="/">
	<span class="label label-succes">
	<i class="icon icon-arrow-left icon-white"></i> Voltar 
	</span>
</a>

""")))})))}
    }
    
    def render(sala:Sala): play.api.templates.HtmlFormat.Appendable = apply(sala)
    
    def f:((Sala) => play.api.templates.HtmlFormat.Appendable) = (sala) => apply(sala)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 13 00:14:32 BRST 2015
                    SOURCE: C:/Users/Caio/Desktop/Sistema-Monitoramento 3/app/views/components/sala.scala.html
                    HASH: 76032c7fc608b19463f7be33a7cc8e48714499f2
                    MATRIX: 782->1|940->13|968->68|1005->71|1037->95|1076->97|1184->169|1197->173|1226->180|1331->249|1344->253|1373->260|1439->290|1452->294|1479->299|1551->335|1564->339|1597->350|1665->382|1680->388|1733->419
                    LINES: 26->1|32->1|33->4|34->5|34->5|34->5|40->11|40->11|40->11|45->16|45->16|45->16|46->17|46->17|46->17|47->18|47->18|47->18|48->19|48->19|48->19
                    -- GENERATED --
                */
            