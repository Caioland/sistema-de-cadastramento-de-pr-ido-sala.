
package views.html.components

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object editaPredio extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Form[Predio],Predio,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(formPredio: Form[Predio], predio: Predio):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import helper.twitterBootstrap._


Seq[Any](format.raw/*1.44*/("""
"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Editar Livro")/*5.22*/ {_display_(Seq[Any](format.raw/*5.24*/("""
	<div class="page-header">
		<h1> Meus Livros</h1>
	</div>
	
	<h4> Atualizar Livro </h4>
	
	"""),_display_(Seq[Any](/*12.3*/form(routes.Admin.updatePredio(predio.id))/*12.45*/ {_display_(Seq[Any](format.raw/*12.47*/("""
	
	"""),_display_(Seq[Any](/*14.3*/inputText(formPredio("nome"), '_label -> "Nome:"))),format.raw/*14.52*/("""
	"""),_display_(Seq[Any](/*15.3*/inputText(formPredio("numero"), '_label -> "Numero:"))),format.raw/*15.56*/("""
	"""),_display_(Seq[Any](/*16.3*/inputText(formPredio("endereco"), '_label -> "Endereco:"))),format.raw/*16.60*/("""
	"""),_display_(Seq[Any](/*17.3*/inputText(formPredio("campus"), '_label -> "Campus:"))),format.raw/*17.56*/("""
	<div>
	<button type="submit" class="btn btn-primary">Salvar</button>
	</div>


	""")))})),format.raw/*23.3*/("""
""")))})))}
    }
    
    def render(formPredio:Form[Predio],predio:Predio): play.api.templates.HtmlFormat.Appendable = apply(formPredio,predio)
    
    def f:((Form[Predio],Predio) => play.api.templates.HtmlFormat.Appendable) = (formPredio,predio) => apply(formPredio,predio)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 13 00:14:32 BRST 2015
                    SOURCE: C:/Users/Caio/Desktop/Sistema-Monitoramento 3/app/views/components/editaPredio.scala.html
                    HASH: d0efae55ddf325f896eaff00d76df668bc72d65f
                    MATRIX: 804->1|992->43|1020->98|1057->101|1085->121|1124->123|1260->224|1311->266|1351->268|1393->275|1464->324|1503->328|1578->381|1617->385|1696->442|1735->446|1810->499|1930->588
                    LINES: 26->1|32->1|33->4|34->5|34->5|34->5|41->12|41->12|41->12|43->14|43->14|44->15|44->15|45->16|45->16|46->17|46->17|52->23
                    -- GENERATED --
                */
            