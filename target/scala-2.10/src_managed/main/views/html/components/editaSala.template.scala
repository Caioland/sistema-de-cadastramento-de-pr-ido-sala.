
package views.html.components

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object editaSala extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Form[Sala],Sala,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(formSala: Form[Sala], sala: Sala):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import helper.twitterBootstrap._


Seq[Any](format.raw/*1.36*/("""
"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Editar Livro")/*5.22*/ {_display_(Seq[Any](format.raw/*5.24*/("""
	<div class="page-header">
		<h1> Meus Livros</h1>
	</div>
	
	<h4> Atualizar Livro </h4>
	
	"""),_display_(Seq[Any](/*12.3*/form(routes.Admin.updateSala(sala.id))/*12.41*/ {_display_(Seq[Any](format.raw/*12.43*/("""
	
	"""),_display_(Seq[Any](/*14.3*/inputText(formSala("numero"), '_label -> "Numero:"))),format.raw/*14.54*/("""
	"""),_display_(Seq[Any](/*15.3*/inputText(formSala("Tipo"), '_label -> "Tipo:"))),format.raw/*15.50*/("""
	"""),_display_(Seq[Any](/*16.3*/inputText(formSala("Capacidade"), '_label -> "Capacidade:"))),format.raw/*16.62*/("""
	"""),_display_(Seq[Any](/*17.3*/select(
			formSala("predio.id"),
			Predio.find.all.map{ v => v.id.toString -> v.nome}, '_label-> "Predio"))),format.raw/*19.75*/("""
			
	<div>
	<button type="submit" class="btn btn-primary">Salvar</button>
	</div>


	""")))})),format.raw/*26.3*/("""
""")))})))}
    }
    
    def render(formSala:Form[Sala],sala:Sala): play.api.templates.HtmlFormat.Appendable = apply(formSala,sala)
    
    def f:((Form[Sala],Sala) => play.api.templates.HtmlFormat.Appendable) = (formSala,sala) => apply(formSala,sala)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 13 00:14:32 BRST 2015
                    SOURCE: C:/Users/Caio/Desktop/Sistema-Monitoramento 3/app/views/components/editaSala.scala.html
                    HASH: 65eeb22b13f4a8759b8fefc9c8e9733ac4eb07d8
                    MATRIX: 798->1|978->35|1006->90|1043->93|1071->113|1110->115|1246->216|1293->254|1333->256|1375->263|1448->314|1487->318|1556->365|1595->369|1676->428|1715->432|1847->542|1972->636
                    LINES: 26->1|32->1|33->4|34->5|34->5|34->5|41->12|41->12|41->12|43->14|43->14|44->15|44->15|45->16|45->16|46->17|48->19|55->26
                    -- GENERATED --
                */
            