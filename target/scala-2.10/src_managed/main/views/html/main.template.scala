
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object main extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[String,Html,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(title: String)(content: Html):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.32*/("""

<!DOCTYPE html>
<!-- TEMPLATE PADRAO --- APLICATIVOS BY ANDRE F. M. BATISTA -->
<html>

<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<meta name="DC.Title" content=" Sistema de cadastramento de Predio-Sala.">
<meta name="DC.Creator" content="Caio Landolphi">
<meta name="DC.Subject" content="monitoramento, consulta, FMU, endereco, andar, sistema, gerenciador, controle, sala, preio">
<meta name="DC.Description" content="Sistema de cadastramento de Predio-Sala:

Este sistema tem como funcao atraves do CRUD interagir com um ambiente em que possamos controlar quantidade de predios e salas de uma universidade, podendo cadastrar predios da faculdade, enderecos, salas existentes, laboratoios, entre outros, para que exista um banco de dados que possua controle e poder de consulta para informar ao usuario.">
<meta name="DC.Publisher" content="Caio Landolphi">
<meta name="DC.Contributor" content="Faculdades Metropolitanas Unidas - FMU">
<meta name="DC.Date" content="2015/10/30">
<meta name="DC.Format" content="Software">
<meta name="DC.Language" content="PT-BR">

    <head>
    	<!-- define o titulo da pagina -->
        <title>"""),_display_(Seq[Any](/*22.17*/title)),format.raw/*22.22*/("""</title>
      
 <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*24.47*/routes/*24.53*/.Assets.at("stylesheets/main.css"))),format.raw/*24.87*/("""">
 <link href=""""),_display_(Seq[Any](/*25.15*/routes/*25.21*/.Assets.at("bootstrap/css/bootstrap.min.css"))),format.raw/*25.66*/("""" rel="stylesheet" media="screen">
 <link rel="shortcut icon" type="image/png" href=""""),_display_(Seq[Any](/*26.52*/routes/*26.58*/.Assets.at("images/favicon.png"))),format.raw/*26.90*/("""">
 <script src=""""),_display_(Seq[Any](/*27.16*/routes/*27.22*/.Assets.at("javascripts/jquery-1.9.1.min.js"))),format.raw/*27.67*/("""" type="text/javascript"></script>
   </head>
   
   <body background="http://images6.fanpop.com/image/photos/36100000/Christmas-image-christmas-36118465-1920-1200.jpg">
    
    <!-- Para exibicao de conteudo flash 
     	 Por padrão, existem duas areas: success e error
     	 Ambas contam com um link capaz de fazer estas div sumirem
    
    --> 
    
    	
    """),_display_(Seq[Any](/*39.6*/if(flash.containsKey("success"))/*39.38*/{_display_(Seq[Any](format.raw/*39.39*/("""
		<div class="alert alert-success">
		<a class="close" data-dismiss="alert">x</a>
		"""),_display_(Seq[Any](/*42.4*/flash/*42.9*/.get("success"))),format.raw/*42.24*/("""
		</div>
	""")))})),format.raw/*44.3*/("""
	"""),_display_(Seq[Any](/*45.3*/if(flash.containsKey("error"))/*45.33*/{_display_(Seq[Any](format.raw/*45.34*/("""
		<div class="alert alert-error">
		<a class="close" data-dismiss="alert">x</a>
		"""),_display_(Seq[Any](/*48.4*/flash/*48.9*/.get("error"))),format.raw/*48.22*/("""
		</div>
	""")))})),format.raw/*50.3*/("""
	<!-- fim conteudp flash -->
	
	
	
	<!-- BARRA TOPO -->
	
	"""),_display_(Seq[Any](/*57.3*/components/*57.13*/.topbar())),format.raw/*57.22*/("""
	
	
			
	</p>
	
	 <!--
	<div class="hero-unit">
		<h1> NOME DA APLICACAO </h1> 	
	</div>
		
	<span class="label label-warning"> Atenção: sistema em fase de testes </span>
	
	</p> -->
	
	<!-- Divisao da Pagina em 2 partes -->	

	<div class="container-fluid">
		<div class="row-fluid">	
                 
			"""),_display_(Seq[Any](/*77.5*/if(session.contains("connected"))/*77.38*/ {_display_(Seq[Any](format.raw/*77.40*/("""
					"""),_display_(Seq[Any](/*78.7*/components/*78.17*/.menu())),format.raw/*78.24*/("""
			""")))})),format.raw/*79.5*/("""
			<div class="span8">
				<!-- BODY CONTENT -->
				"""),_display_(Seq[Any](/*82.6*/content)),format.raw/*82.13*/("""
			</div>
		</div>
	</div>

      <hr>

	<!-- RODAPE  -->
            <footer>
	    <p>&copy; 2015 <span class="label label-important"> FMU </span>  Disciplina de Linguagens e Tecnicas de Programacao.</p>
		    </footer>


</body>

</html>
"""))}
    }
    
    def render(title:String,content:Html): play.api.templates.HtmlFormat.Appendable = apply(title)(content)
    
    def f:((String) => (Html) => play.api.templates.HtmlFormat.Appendable) = (title) => (content) => apply(title)(content)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 13 00:14:32 BRST 2015
                    SOURCE: C:/Users/Caio/Desktop/Sistema-Monitoramento 3/app/views/main.scala.html
                    HASH: 771ed7d2051e2c8f5d44b8b398411490fb14a446
                    MATRIX: 778->1|902->31|2093->1186|2120->1191|2218->1253|2233->1259|2289->1293|2342->1310|2357->1316|2424->1361|2546->1447|2561->1453|2615->1485|2669->1503|2684->1509|2751->1554|3153->1921|3194->1953|3233->1954|3354->2040|3367->2045|3404->2060|3447->2072|3485->2075|3524->2105|3563->2106|3682->2190|3695->2195|3730->2208|3773->2220|3869->2281|3888->2291|3919->2300|4262->2608|4304->2641|4344->2643|4386->2650|4405->2660|4434->2667|4470->2672|4560->2727|4589->2734
                    LINES: 26->1|29->1|50->22|50->22|52->24|52->24|52->24|53->25|53->25|53->25|54->26|54->26|54->26|55->27|55->27|55->27|67->39|67->39|67->39|70->42|70->42|70->42|72->44|73->45|73->45|73->45|76->48|76->48|76->48|78->50|85->57|85->57|85->57|105->77|105->77|105->77|106->78|106->78|106->78|107->79|110->82|110->82
                    -- GENERATED --
                */
            