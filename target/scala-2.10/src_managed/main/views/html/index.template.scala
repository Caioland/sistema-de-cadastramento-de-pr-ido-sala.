
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object index extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Form[Predio],Form[Sala],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(formPredio: Form[Predio], formSala: Form[Sala]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import helper.twitterBootstrap._


Seq[Any](format.raw/*1.50*/("""
"""),format.raw/*4.1*/("""


"""),_display_(Seq[Any](/*7.2*/main("FMU - Sistema de cadastramento de Pr�dio-Sala.")/*7.56*/ {_display_(Seq[Any](format.raw/*7.58*/("""


	<center> <img src="http://www.posfmu.com.br/template/posfmu/images/fmu_logo.jpg"/></center>
	

	<div class = "page-header">
		<h1> Meus Registros </h1>
	</div>
	
		<h4> <p align="center"> Predios Cadastrados: </p> </h4>
    	
    	<ul>
    	"""),_display_(Seq[Any](/*20.7*/for(predio <- Predio.find.all()) yield /*20.39*/{_display_(Seq[Any](format.raw/*20.40*/("""
    	 <p align="center"> """),_display_(Seq[Any](/*21.27*/predio/*21.33*/.nome)),format.raw/*21.38*/(""" Endereco: """),_display_(Seq[Any](/*21.50*/predio/*21.56*/.endereco)),format.raw/*21.65*/(""" ("""),_display_(Seq[Any](/*21.68*/predio/*21.74*/.numero)),format.raw/*21.81*/(""") </p> </li>
    	<p align="center"> <a span class="label label-info" href=""""),_display_(Seq[Any](/*22.65*/routes/*22.71*/.Admin.detalhaPredio(predio.id))),format.raw/*22.102*/("""" >Ver Detalhes</a> 
		<a href=""""),_display_(Seq[Any](/*23.13*/routes/*23.19*/.Admin.editaPredio(predio.id))),format.raw/*23.48*/(""""> <span class="label label-warning"> Editar </span></a>
		<a href=""""),_display_(Seq[Any](/*24.13*/routes/*24.19*/.Admin.deletaPredio(predio.id))),format.raw/*24.49*/(""""> <span class="label label-important"> Deletar </span></a> </p>
		
    	""")))})),format.raw/*26.7*/("""
    	</ul>	
	
"""),_display_(Seq[Any](/*29.2*/form(routes.Admin.addpredio())/*29.32*/ {_display_(Seq[Any](format.raw/*29.34*/("""

	
	<h4> Cadastro de Predios </h4>
	
	<h4> Novo Predio </h4>
	
	"""),_display_(Seq[Any](/*36.3*/form(routes.Admin.addpredio())/*36.33*/ {_display_(Seq[Any](format.raw/*36.35*/("""
	
		"""),_display_(Seq[Any](/*38.4*/inputText(formPredio("nome"),'_label -> "Nome:"))),format.raw/*38.52*/("""
		"""),_display_(Seq[Any](/*39.4*/inputText(formPredio("endereco"),'_label -> "Endereco:"))),format.raw/*39.60*/("""
		"""),_display_(Seq[Any](/*40.4*/inputText(formPredio("numero"),'_label -> "Numero:"))),format.raw/*40.56*/("""
		"""),_display_(Seq[Any](/*41.4*/inputText(formPredio("campus"),'_label -> "Campus:"))),format.raw/*41.56*/("""
		"""),_display_(Seq[Any](/*42.4*/inputText(formPredio("andar"),'_label -> "Andares:"))),format.raw/*42.56*/("""
		<input type="submit" class="btn btn-success" value="Cadastrar Predio">
		<input class="btn" type="reset" value="Limpar" />
	
	""")))})),format.raw/*46.3*/("""
	

	                
	
	</u1>

	
""")))})),format.raw/*54.2*/("""
	
<h4> <p align="center"> Salas Cadastradas: </p> </h4>
	
	<ul>
		"""),_display_(Seq[Any](/*59.4*/for(sala <- Sala.find.all()) yield /*59.32*/{_display_(Seq[Any](format.raw/*59.33*/("""
		<p align="center"> Sala: """),_display_(Seq[Any](/*60.29*/sala/*60.33*/.numero)),format.raw/*60.40*/(""" Tipo: ("""),_display_(Seq[Any](/*60.49*/sala/*60.53*/.tipo)),format.raw/*60.58*/(""") Predio: </b> """),_display_(Seq[Any](/*60.74*/Predio/*60.80*/.find.byId(sala.predio.id).nome)),format.raw/*60.111*/(""" </p>
		<p align="center"> <a span class="label label-info" href=""""),_display_(Seq[Any](/*61.62*/routes/*61.68*/.Admin.detalhaSala(sala.id))),format.raw/*61.95*/("""" >Ver Detalhes</a>
		<a href=""""),_display_(Seq[Any](/*62.13*/routes/*62.19*/.Admin.editaSala(sala.id))),format.raw/*62.44*/(""""> <span class="label label-warning"> Editar </span></a>
		<a href=""""),_display_(Seq[Any](/*63.13*/routes/*63.19*/.Admin.deletaSala(sala.id))),format.raw/*63.45*/(""""> <span class="label label-important"> Deletar </span></a> </p>
		
		""")))})),format.raw/*65.4*/("""
	</ul>	


	
"""),_display_(Seq[Any](/*70.2*/form(routes.Admin.addsala())/*70.30*/ {_display_(Seq[Any](format.raw/*70.32*/("""

	<h4> Cadastro de Salas </h4>
		
		"""),_display_(Seq[Any](/*74.4*/inputText(formSala("numero"),'_label -> "Numero:"))),format.raw/*74.54*/("""
		"""),_display_(Seq[Any](/*75.4*/inputText(formSala("tipo"),'_label -> "Tipo:"))),format.raw/*75.50*/("""
		"""),_display_(Seq[Any](/*76.4*/inputText(formSala("capacidade"),'_label -> "Capacidade:"))),format.raw/*76.62*/("""
		"""),_display_(Seq[Any](/*77.4*/select(
				formSala("predio.id"),
				Predio.find.all().map{ s => s.id.toString -> s.nome},
				'_label -> "Predio",
				'_default -> "-- Predio --"))),format.raw/*81.33*/("""
	   <input type="submit" class="btn btn-success" value="Cadastrar Sala">
	   <input class="btn" type="reset" value="Limpar" />



	
""")))})),format.raw/*88.2*/("""
	

""")))})))}
    }
    
    def render(formPredio:Form[Predio],formSala:Form[Sala]): play.api.templates.HtmlFormat.Appendable = apply(formPredio,formSala)
    
    def f:((Form[Predio],Form[Sala]) => play.api.templates.HtmlFormat.Appendable) = (formPredio,formSala) => apply(formPredio,formSala)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 13 00:14:31 BRST 2015
                    SOURCE: C:/Users/Caio/Desktop/Sistema-Monitoramento 3/app/views/index.scala.html
                    HASH: b27d20c588cd7c1d86ead8a3a04da4419db5fd01
                    MATRIX: 791->1|983->49|1010->101|1048->105|1110->159|1149->161|1430->407|1478->439|1517->440|1580->467|1595->473|1622->478|1670->490|1685->496|1716->505|1755->508|1770->514|1799->521|1912->598|1927->604|1981->635|2050->668|2065->674|2116->703|2221->772|2236->778|2288->808|2393->882|2444->898|2483->928|2523->930|2624->996|2663->1026|2703->1028|2744->1034|2814->1082|2853->1086|2931->1142|2970->1146|3044->1198|3083->1202|3157->1254|3196->1258|3270->1310|3431->1440|3497->1475|3600->1543|3644->1571|3683->1572|3748->1601|3761->1605|3790->1612|3835->1621|3848->1625|3875->1630|3927->1646|3942->1652|3996->1683|4099->1750|4114->1756|4163->1783|4231->1815|4246->1821|4293->1846|4398->1915|4413->1921|4461->1947|4563->2018|4612->2032|4649->2060|4689->2062|4762->2100|4834->2150|4873->2154|4941->2200|4980->2204|5060->2262|5099->2266|5271->2416|5436->2550
                    LINES: 26->1|32->1|33->4|36->7|36->7|36->7|49->20|49->20|49->20|50->21|50->21|50->21|50->21|50->21|50->21|50->21|50->21|50->21|51->22|51->22|51->22|52->23|52->23|52->23|53->24|53->24|53->24|55->26|58->29|58->29|58->29|65->36|65->36|65->36|67->38|67->38|68->39|68->39|69->40|69->40|70->41|70->41|71->42|71->42|75->46|83->54|88->59|88->59|88->59|89->60|89->60|89->60|89->60|89->60|89->60|89->60|89->60|89->60|90->61|90->61|90->61|91->62|91->62|91->62|92->63|92->63|92->63|94->65|99->70|99->70|99->70|103->74|103->74|104->75|104->75|105->76|105->76|106->77|110->81|117->88
                    -- GENERATED --
                */
            