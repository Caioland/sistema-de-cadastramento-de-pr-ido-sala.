// @SOURCE:C:/Users/Caio/Desktop/Sistema-Monitoramento 3/conf/routes
// @HASH:a1a6ef6bb943b6797d9e7fee3d2bc0b91c2cac0f
// @DATE:Fri Nov 13 00:14:30 BRST 2015

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString


// @LINE:26
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
package controllers {

// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:6
class ReverseAdmin {
    

// @LINE:14
// @LINE:13
def addsala(): Call = {
   () match {
// @LINE:13
case () if true => Call("GET", _prefix + { _defaultPrefix } + "addsala")
                                                        
// @LINE:14
case () if true => Call("POST", _prefix + { _defaultPrefix } + "addsala")
                                                        
   }
}
                                                

// @LINE:24
def deletaPredio(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "predio/" + implicitly[PathBindable[Long]].unbind("id", id) + "/del")
}
                                                

// @LINE:23
def updatePredio(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "predio/" + implicitly[PathBindable[Long]].unbind("id", id) + "/update")
}
                                                

// @LINE:22
def editaPredio(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "predio/" + implicitly[PathBindable[Long]].unbind("id", id) + "/edit")
}
                                                

// @LINE:18
def editaSala(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "sala/" + implicitly[PathBindable[Long]].unbind("id", id) + "/edit")
}
                                                

// @LINE:16
// @LINE:15
def addpredio(): Call = {
   () match {
// @LINE:15
case () if true => Call("GET", _prefix + { _defaultPrefix } + "addpredio")
                                                        
// @LINE:16
case () if true => Call("POST", _prefix + { _defaultPrefix } + "addpredio")
                                                        
   }
}
                                                

// @LINE:19
def updateSala(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "sala/" + implicitly[PathBindable[Long]].unbind("id", id) + "/update")
}
                                                

// @LINE:12
def sobre(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "sobre")
}
                                                

// @LINE:21
def detalhaPredio(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "predio/" + implicitly[PathBindable[Long]].unbind("id", id) + "/view")
}
                                                

// @LINE:20
def deletaSala(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "sala/" + implicitly[PathBindable[Long]].unbind("id", id) + "/del")
}
                                                

// @LINE:6
def index(): Call = {
   Call("GET", _prefix)
}
                                                

// @LINE:17
def detalhaSala(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "sala/" + implicitly[PathBindable[Long]].unbind("id", id) + "/view")
}
                                                
    
}
                          

// @LINE:26
class ReverseAssets {
    

// @LINE:26
def at(file:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                                                
    
}
                          

// @LINE:11
// @LINE:10
// @LINE:9
class ReverseApplication {
    

// @LINE:11
def logout(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "logout")
}
                                                

// @LINE:10
def authenticate(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "login")
}
                                                

// @LINE:9
def login(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "login")
}
                                                
    
}
                          
}
                  


// @LINE:26
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
package controllers.javascript {

// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:6
class ReverseAdmin {
    

// @LINE:14
// @LINE:13
def addsala : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.addsala",
   """
      function() {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "addsala"})
      }
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "addsala"})
      }
      }
   """
)
                        

// @LINE:24
def deletaPredio : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.deletaPredio",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "predio/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/del"})
      }
   """
)
                        

// @LINE:23
def updatePredio : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.updatePredio",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "predio/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/update"})
      }
   """
)
                        

// @LINE:22
def editaPredio : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.editaPredio",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "predio/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/edit"})
      }
   """
)
                        

// @LINE:18
def editaSala : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.editaSala",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "sala/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/edit"})
      }
   """
)
                        

// @LINE:16
// @LINE:15
def addpredio : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.addpredio",
   """
      function() {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "addpredio"})
      }
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "addpredio"})
      }
      }
   """
)
                        

// @LINE:19
def updateSala : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.updateSala",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "sala/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/update"})
      }
   """
)
                        

// @LINE:12
def sobre : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.sobre",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "sobre"})
      }
   """
)
                        

// @LINE:21
def detalhaPredio : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.detalhaPredio",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "predio/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/view"})
      }
   """
)
                        

// @LINE:20
def deletaSala : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.deletaSala",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "sala/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/del"})
      }
   """
)
                        

// @LINE:6
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
   """
)
                        

// @LINE:17
def detalhaSala : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.detalhaSala",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "sala/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/view"})
      }
   """
)
                        
    
}
              

// @LINE:26
class ReverseAssets {
    

// @LINE:26
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        
    
}
              

// @LINE:11
// @LINE:10
// @LINE:9
class ReverseApplication {
    

// @LINE:11
def logout : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.logout",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logout"})
      }
   """
)
                        

// @LINE:10
def authenticate : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.authenticate",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
      }
   """
)
                        

// @LINE:9
def login : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.login",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
      }
   """
)
                        
    
}
              
}
        


// @LINE:26
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
package controllers.ref {


// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:6
class ReverseAdmin {
    

// @LINE:13
def addsala(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.addsala(), HandlerDef(this, "controllers.Admin", "addsala", Seq(), "GET", """""", _prefix + """addsala""")
)
                      

// @LINE:24
def deletaPredio(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.deletaPredio(id), HandlerDef(this, "controllers.Admin", "deletaPredio", Seq(classOf[Long]), "GET", """""", _prefix + """predio/$id<[^/]+>/del""")
)
                      

// @LINE:23
def updatePredio(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.updatePredio(id), HandlerDef(this, "controllers.Admin", "updatePredio", Seq(classOf[Long]), "POST", """""", _prefix + """predio/$id<[^/]+>/update""")
)
                      

// @LINE:22
def editaPredio(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.editaPredio(id), HandlerDef(this, "controllers.Admin", "editaPredio", Seq(classOf[Long]), "GET", """""", _prefix + """predio/$id<[^/]+>/edit""")
)
                      

// @LINE:18
def editaSala(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.editaSala(id), HandlerDef(this, "controllers.Admin", "editaSala", Seq(classOf[Long]), "GET", """""", _prefix + """sala/$id<[^/]+>/edit""")
)
                      

// @LINE:15
def addpredio(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.addpredio(), HandlerDef(this, "controllers.Admin", "addpredio", Seq(), "GET", """""", _prefix + """addpredio""")
)
                      

// @LINE:19
def updateSala(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.updateSala(id), HandlerDef(this, "controllers.Admin", "updateSala", Seq(classOf[Long]), "POST", """""", _prefix + """sala/$id<[^/]+>/update""")
)
                      

// @LINE:12
def sobre(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.sobre(), HandlerDef(this, "controllers.Admin", "sobre", Seq(), "GET", """""", _prefix + """sobre""")
)
                      

// @LINE:21
def detalhaPredio(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.detalhaPredio(id), HandlerDef(this, "controllers.Admin", "detalhaPredio", Seq(classOf[Long]), "GET", """""", _prefix + """predio/$id<[^/]+>/view""")
)
                      

// @LINE:20
def deletaSala(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.deletaSala(id), HandlerDef(this, "controllers.Admin", "deletaSala", Seq(classOf[Long]), "GET", """""", _prefix + """sala/$id<[^/]+>/del""")
)
                      

// @LINE:6
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.index(), HandlerDef(this, "controllers.Admin", "index", Seq(), "GET", """ Home page""", _prefix + """""")
)
                      

// @LINE:17
def detalhaSala(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.detalhaSala(id), HandlerDef(this, "controllers.Admin", "detalhaSala", Seq(classOf[Long]), "GET", """""", _prefix + """sala/$id<[^/]+>/view""")
)
                      
    
}
                          

// @LINE:26
class ReverseAssets {
    

// @LINE:26
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ Map static resources from the /public folder to the /assets URL path""", _prefix + """assets/$file<.+>""")
)
                      
    
}
                          

// @LINE:11
// @LINE:10
// @LINE:9
class ReverseApplication {
    

// @LINE:11
def logout(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.logout(), HandlerDef(this, "controllers.Application", "logout", Seq(), "GET", """""", _prefix + """logout""")
)
                      

// @LINE:10
def authenticate(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.authenticate(), HandlerDef(this, "controllers.Application", "authenticate", Seq(), "POST", """""", _prefix + """login""")
)
                      

// @LINE:9
def login(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.login(), HandlerDef(this, "controllers.Application", "login", Seq(), "GET", """ Authentication""", _prefix + """login""")
)
                      
    
}
                          
}
        
    